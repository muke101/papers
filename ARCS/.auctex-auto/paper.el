(TeX-add-style-hook
 "paper"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("llncs" "runningheads")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("hyperref" "hidelinks")))
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "macros"
    "llncs"
    "llncs10"
    "hyperref"
    "graphicx"
    "listings"
    "color")
   (LaTeX-add-labels
    "sec:ooo"
    "fig:lsq"
    "fig:pnd-example"
    "sec:llvm"
    "sec:alg"
    "sec:limitations"
    "fig:limitations"
    "sec:communication"
    "sec:gem5"
    "sec:cpu"
    "table:cpu"
    "sec:workflow"
    "sec:evaluation"
    "fig:lookups"
    "sec:cpi"
    "fig:cpi"
    "sec:discussion"
    "sec:relatedwork"
    "sec:conclusion"
    "sec:futurework")
   (LaTeX-add-bibliographies
    "bibtex")
   (LaTeX-add-color-definecolors
    "mygray"))
 :latex)

