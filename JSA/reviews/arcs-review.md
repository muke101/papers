## REVIEW 1

**SCORE: 1 (weak accept)**

This paper presents an approach to tag at compilation time a memory
load as non-dependent and hence avoid the lookup at the memory
dependence predictor at runtime.

The approach is interesting research-wise but to my view of rather
limited potential impact due to the quite large cost (in terms of
intrusiveness as it requires changes in the ISA) for a rather low
performance benefit. On the other hand, the authors mention a
potential in energy reduction which is not quantified but seems like
an interesting prospect.


## REVIEW 2 

**SCORE: 0 (borderline paper)**


The paper presents a solution to improve performance in OoO processors
by improving the memory dependence prediction in case a memory load is
made to a target address that cannot be targeted by any previous store
currently on the fly. The idea is to perform a static analysis as part
of the compiler passes to detect such instructions and mark them so
the compiler emits a particular version of the instruction. When such
an instruction reaches the memory load stage, it skips the memory
dependency predictor (unlike the non-marked version of the
instruction), and the load is allowed to proceed right away. In its
current version, the analysis uses loop nests as scope, which may
create some situations in which the instructions are wrongfully
marked, as pending dependencies from stores happening before a loop
nest may exist. When this happens, the memory order violation is
detected later, when the store commits, and the load, as well as all
subsequent instructions must be rolled back . The evaluation section
shows that in most of the benchmarks this has little impact on the
performance, except for 2 benchmarks in which the solution leads to
lower performance than the baseline. The implementation of the
solution is still partial (not all load instruction variants are
implemented in their marked version), and the evaluation relies on a
very small set of benchmarks, for which the gain is not significant.

Overall the paper is nice to read, the problem is interesting and the
solution seems promising. Should the paper be accepted, I think it
could foster interesting discussions at the conference. At the same
time, the lack of maturity, especially in the experiments, makes any
conclusion fragile at this time.

## REVIEW 3 

**SCORE: 1 (weak accept)**	

The paper "Improving Memory Dependence Prediction with Static
Analysis" by Panayi et al. is a paper proposing the idea of using the
LLVM compiler toolchain to label specific load instructions in an
arbitrary "for loop". The labeled load instructions would then
circumvent entirely the memory dependence predictor, improving
performance and reducing power consumption. This paper is well written
throughout. Each section provided meaningful insight into not only the
topic of the paper, but also the thought process of the authors.

The paper does not present anything novel in the field of computer
architecture; rather the authors use already existing techniques
provided by LLVM contributors to implement a static memory dependence
predictor (at compile time, i.e. embedding static predictions into the
binary themselves) to increase performance of a chip with little
overhead or modifications. As such, we interpret this paper more along
the lines of a computer engineering paper rather than a computer
research paper. From that vantage point, no major criticisms about the
paper are apropos, only a couple of minor critiques:

1. It took us a while to understand the meaning as to why the authors
   are still implementing rollback logic for labeled load instructions
   despite the well detailed listing 1.2 that provides an edge case
   example showing the need for the speculative checking in the commit
   stage. The main reason for the confusion was because the listing
   showed a function that could be trivially fused (by the compiler)
   such that the b array need not exist and hence may not even
   generate loads and stores for the b array in the assembly. We feel
   that a better way to show the edge case in C would be to move the
   initializing logic for the b array (the first for loop) to a
   separate function (that way the compiler likely won't fuse the
   loops), or better yet, provide some example ARM assembly as well to
   detail the issue. While the beauty of C is preferred in most cases,
   for more nitty gritty instruction level details, we feel that
   assembly code would provide more informative examples.

2. We realize that time constraints couldn't allow for more benchmark
   runs, but would have liked to see a couple more benchmarks run
   (perhaps in the future work?).

3. We are unsure if it's mentioned explicitly in the paper, but we
   were curious about whether or not the additional passes the author
   implemented had a significant impact on compile time.

4. Affiliation 1 of the authors might be misspelled (perhaps "College"
   instead of "Collage"?)

Overall, the paper is a well written engineering paper.
