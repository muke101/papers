Manuscript Number: JSA-D-24-01038   

Reducing False Memory Dependencies with Static Analysis 

Dear Mr Panayi,

Thank you for submitting your manuscript to Journal of Systems Architecture. 

I regret to inform you that the reviewers recommend against publishing your manuscript, and I must therefore reject it. My comments, and any reviewer comments, are below.   

We appreciate you submitting your manuscript to Journal of Systems Architecture and thank you for giving us the opportunity to consider your work. 

Kind regards,   

Z. Gu   
Editor-in-Chief  

Journal of Systems Architecture 

## Editor and Reviewer comments

Your manuscript shows an unacceptable level of overlap with prior
publications. There are numerous complete sentences and paragraphs
that are virtually unchanged. In the present form, the originality of
the study cannot be assessed.

## Reviewer #1

This paper is an extended version of a previously published work and
presents an approach to decrease the queries to the Memory Dependence
Predictor (MDP) a microarchitectural component that predicts data
dependencies and enhances OoO execution. The approach is based on a
static analysis (compiler) pass that detects read operations within
loops with high probability of carrying a dependence. The approach has
satisfactory results with reductions in MDP accesses this translating
to non-negligible performance gains. Potential gains in power are
mentioned but not further analyzed or evaluated.

The paper is solid, well written and its performance results are
analyzed with proper detail.

One item that I think needs improvement is a discussion about the
actual implications of a real implementation. The authors state that
new opcodes are needed to implement the approach and discuss how they
are able to "trick" LLVM and Gem5 in order not to implement new
opcodes in simulation. However, in real system new opcodes might have
both hardware and latency costs. The authors could extend section 2.3
with a discussion on these implications.

## Reviewer #2

The paper extends a ARCS24 paper about employing static
analysis to improve performance in out-of-order processors by
bypassing the memory dependence predictor (MDP). The idea is to re-use
the result of classic compiler passes in order to detect memory load
instructions that can be statically determined not to have an address
dependency with a pending store for which the address is not yet
computed. In order for the processor to bypass the MDP for these
instructions, the authors propose to extend the instruction set
architecture to include "bypass" loads. The idea is that during
compilation the compiler marks those loads that are certain to not
have a dependency with a previous store whose target address is still
undetermined, so that during the instruction selection pass, the
selected instruction be a "bypass" load that the MDP does not
recognize as a load during the execution, thus effectively bypassing
it. The authors claim that this reduces the intrusiveness of the
method as the MDP does not have to be modified. They implement their
method using LLVM and gem5, but instead of particular instructions,
they rely on the addresses of the instructions to tell the simulator
to bypass the prediction on the selected instructions. They propose an
evaluation to show that performance can be increased significantly on
certain benchmarks.

My opinion is that the contents of the paper simply do not match the
requirements of a Q1 journal paper, special edition or not.  First,
the extension does not add sufficiently original material compared to
the ARCS version. The evaluation has been modified with architecture
models that are calibrated using partial information of real
processors, but the analysis has not been improved. It relies only on
already existing analyses (which was already the case for the ARCS
paper), and does not propose original, dedicated algorithms to improve
the analysis for the problem at hand. Overall, the reader does not get
new insights from this paper that she/he did not already get from the
ARCS paper.  Second, the claim that the method is not intrusive seems
to be a response to one of the original reviews. However, this claim
is not convincing: in order to employ this method one has to add new
instructions to the target ISA, modify the instruction decoder of the
processor accordingly, and include these instructions in the compiler
passes. This seems pretty intrusive.  Third, the reported gains are
only significant for three benchmarks, which are three variants of the
same benchmark. The authors claim that simulation takes time and that
they could not run more benchmarks before the paper deadline. I
believe this was already a claim in the ARCS paper. However, this is
not a scientifically valid argument: the correct way to proceed is to
take the time to perform all necessary experiments, then try to
publish a paper.

For all these reasons I propose to reject this paper.

## Reviewer #3

This paper is an extended version of an paper from ARCS24.  The
authors propose to label load instructions according to compiler
analysis for improved behavior of the memory dependence predictor used
for better speculation in modern out-of-order processors.

The extensions consist of

(1) a better approach to forward labels from the compiler to
the GEM5 simulator without using new opcodes but a file with
load instruction addresses, avoiding the issue to provide new
opcodes for all load instructions in ARM (which only partly
was done in the ARCS paper),

(2) re-running the measurements for a new set of 4, more realistic
CPU configurations with parameters from real implementations
(instead of 3, somehow arbitrarily chosen configs in ARCS paper),
as well as extending the number of benchmarks evaluated (14 -> 18);

(3) Significantly extended discussion of results including adding
new Figures 4,5,6 to show more details of simulated results.

In addition, the rest of the text got improved and adjusted in
quite some parts.

Other remarks:

- abstract/intro: "without additional cost...instruction bandwidth".
  This is not correct when you need to introduce new load opcodes in
  the instruction encoding.

- 2.3 "emit labelled opcodes" - this step is not needed with your new
  approach of generating a file with instruction addresses?

- 2.3 "Our method ... allows multiple sets of _labels_ for a single
  binary" Can you add a sentence about where this is used / why this
  is useful?

- Table 1: please shrink to text width

- 3.4 "we compile two sets ... with labelled .. and one without" This
  was the ARCS approach and is wrong now? Also later in 3.4.

- 4 Eval: the benchmarks are named differently in the text vs. Figs
  2-6.  This is really confusing. E.g. "605.mcf_s" vs "mcf.0". Needs
  to be fixed

- Fig2: how is it possible that the labelled run of "mcf.0" does more
  MDP lookups?  To my understanding it always should go down?

- 4.3: "size of Store Sets tables ... plays dominant role". This is
  not shown in any of your figures, so currently this is a claim
  without provided measurement.  (as all 4 CPU configs are different
  in many parameters). It would be nice to show this effect at least
  for a CPU config/benchmark combo where this is obvious.

- 4.3. mentions a Table 4.3, which does not exist? Is this Table 2?
