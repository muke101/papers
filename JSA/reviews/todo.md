## REVIEW 1

- **TODO**. *"On the other hand, the authors mention a
  potential in energy reduction which is not quantified but seems like
  an interesting prospect."*

  **ACTION:** tbc


## REVIEW 2 

**SCORE: 0 (borderline paper)**


- **TODO**. *"The evaluation section shows that in most of the
  benchmarks this has little impact on the performance, except for 2
  benchmarks in which the solution leads to lower performance than the
  baseline."*

  **ACTION:** tcb

- **TODO**. *"The implementation of the solution is still partial (not
  all load instruction variants are implemented in their marked
  version), and the evaluation relies on a very small set of
  benchmarks, for which the gain is not significant."*

  **ACTION:** tbc
  
## REVIEW 3 

**SCORE: 1 (weak accept)**	

- **TODO**. *"1. It took us a while to understand the meaning as to
   why the authors are still implementing rollback logic for labeled
   load instructions despite the well detailed listing 1.2 that
   provides an edge case example showing the need for the speculative
   checking in the commit stage. The main reason for the confusion was
   because the listing showed a function that could be trivially fused
   (by the compiler) such that the b array need not exist and hence
   may not even generate loads and stores for the b array in the
   assembly. We feel that a better way to show the edge case in C
   would be to move the initializing logic for the b array (the first
   for loop) to a separate function (that way the compiler likely
   won't fuse the loops), or better yet, provide some example ARM
   assembly as well to detail the issue. While the beauty of C is
   preferred in most cases, for more nitty gritty instruction level
   details, we feel that assembly code would provide more informative
   examples."*
   
  **ACTION:** tbc   

- **TODO**. *"2. We realize that time constraints couldn't allow for
   more benchmark runs, but would have liked to see a couple more
   benchmarks run (perhaps in the future work?)."*

  **ACTION:** tbc

- **TODO**. *"3. We are unsure if it's mentioned explicitly in the
   paper, but we were curious about whether or not the additional
   passes the author implemented had a significant impact on compile
   time."*

  **ACTION:** tbc
  
- **TODO**. *"4. Affiliation 1 of the authors might be misspelled (perhaps "College"
   instead of "Collage"?)"*

  **ACTION:** tbc

